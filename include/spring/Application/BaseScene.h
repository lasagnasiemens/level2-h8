#pragma once

#include <spring\Framework\IScene.h>
#include <qobject.h>
#include <qcustomplot.h>
#include "spring/Application/MonoInput.h"
#include <QAudioInput>

namespace Spring
{
	class BaseScene : public IScene
	{
		Q_OBJECT

	public:
		BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;
		~BaseScene();

  public slots:
      void mp_BackButton();
	  void mf_PlotRandom();
	  void mf_CleanPlot();
	  void mf_StartTimer();
	  void mf_StopTimer();
  private:
		void mp_InitPlotters();
		double mv_dRefreshRate;
		QWidget * centralWidget;
		QCustomPlot* mv_customPlot;
		QTimer mv_Timer;
		QAudioInput *audioInput;
		MonoInput *inputDevice;
	};
}
