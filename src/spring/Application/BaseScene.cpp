#include <spring\Application\BaseScene.h>

#include "ui_base_scene.h"
namespace Spring
{
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}
	void BaseScene::createScene()
	{
		//create the UI
		const auto ui = std::make_shared<Ui_baseScene>();
		ui->setupUi(m_uMainWindow.get());

		//connect btn's release signal to defined slot
		QObject::connect(ui->backButton, SIGNAL(released()), this, SLOT(mp_BackButton()));

		//setting a temporary new title
		m_uMainWindow->setWindowTitle(QString("new title goes here"));

		//setting centralWidget
		centralWidget = ui->centralwidget;

		//Get the title from transient data
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);

		m_uMainWindow->setWindowTitle(QString(appName.c_str()));
		//Ploting
		mv_customPlot = ui->widget;
		// Initialize plotters
		mp_InitPlotters();

		//connect start button
		QObject::connect(ui->startButton, SIGNAL(released()), this, SLOT(mf_StartTimer()));
		//connect stop button
		QObject::connect(ui->stopButton, SIGNAL(released()), this, SLOT(mf_StopTimer()));
		//Initialize refresh rate (Hz)
		mv_dRefreshRate = boost::any_cast<double>(m_TransientDataCollection["RefreshRate"]);
		//Create timer
		mv_Timer.setInterval(1000 / mv_dRefreshRate);
		QObject::connect(&mv_Timer, SIGNAL(timeout()), this, SLOT(mf_PlotRandom()));
		//Initialize sample rate
		unsigned sampleRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("SampleRate")->second);
		//Initialize Display Time
		double displayTime = boost::any_cast<double>(m_TransientDataCollection.find("DisplayTime")->second);
		//Initialize input Device
		inputDevice = new MonoInput(displayTime, sampleRate);
	}
	void BaseScene::release()
	{
		mf_StopTimer();
		delete centralWidget;
		delete inputDevice;
	}

	BaseScene::~BaseScene()
	{

	}

	void BaseScene::mf_PlotRandom()
	{
		QVector<double> y = inputDevice->vecGetData();
		const int lc_nNoOfSamples = y.length();
		QVector<double> x(lc_nNoOfSamples);
		//Initialize sample rate
		unsigned sampleRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("SampleRate")->second);
		for (int i = 0; i < lc_nNoOfSamples; i++)
		{
			x[i]=(double)i * 1.0 / sampleRate;
		}
		x.resize(y.size());

		mv_customPlot->graph(0)->setData(x, y);
		mv_customPlot->rescaleAxes();
		mv_customPlot->replot();
	}

	void BaseScene::mf_CleanPlot()
	{
		mv_customPlot->graph(0)->data()->clear();
		mv_customPlot->replot();
	}

	void BaseScene::mf_StartTimer()
	{
		audioInput = new QAudioInput(inputDevice->getAudioFormat());
		inputDevice->open(QIODevice::WriteOnly);
		audioInput->start(inputDevice);
		mv_Timer.start();
	}

	void BaseScene::mf_StopTimer()
	{
		mv_Timer.stop();
		audioInput->stop();
		inputDevice->close();
		mf_CleanPlot();
	}

	void BaseScene::mp_InitPlotters()
	{
		// Time plot
		mv_customPlot->setInteraction(QCP::iRangeZoom, true);
		mv_customPlot->setInteraction(QCP::iRangeDrag, true);

		mv_customPlot->addGraph();
		mv_customPlot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);

		mv_customPlot->xAxis->setLabel("sec");
		mv_customPlot->yAxis->setLabel("V");

	}

	void BaseScene::mp_BackButton()
	{
		const std::string c_szNextSceneName = "Initial scene";
		emit SceneChange(c_szNextSceneName);
	}

}
