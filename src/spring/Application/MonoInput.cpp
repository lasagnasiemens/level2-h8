#include "spring/Application/MonoInput.h"
#include <qendian.h>
#include <iostream>

MonoInput::MonoInput(double timeSlice, unsigned sampleRate)
	:dataLength(timeSlice * sampleRate),
	channelBytes(2)
{
	mAudioFormat.setSampleRate(sampleRate);
	mAudioFormat.setChannelCount(1);
	mAudioFormat.setSampleSize(16);
	mAudioFormat.setCodec("audio/pcm");
	mAudioFormat.setByteOrder(QAudioFormat::LittleEndian);
	mAudioFormat.setSampleType(QAudioFormat::SignedInt);

	maxAmplitude = 32767;
}


MonoInput::~MonoInput()
{
}

qint64 MonoInput::writeData(const char *data, qint64 len)
{
	mSamles.clear();
	const auto *ptr = reinterpret_cast<const unsigned char *>(data);
	for (qint64 i = 0; i < len / channelBytes; ++i) {

		double value = qFromLittleEndian<qint16>(ptr);
		
		auto level = float(value) * (5./ maxAmplitude);
		mSamles.push_back(level);
		ptr += channelBytes;
	}

	if (mSamles.size() > dataLength)
		mSamles.remove(0, mSamles.size() - dataLength);

	return len;
}


QAudioFormat MonoInput::getAudioFormat()
{
	return mAudioFormat;
}

QVector<double> MonoInput::vecGetData() const
{
	return mSamles;
}